package sbp.branching;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentMatchers;
import org.mockito.Mock;
import org.mockito.Mockito;
import sbp.common.Utils;

public class MyBranchingTest
{
    @Test
    public void MaxInt_Test()
    {
        final int i1 = 0;
        final int i2 = 100;

        Utils utilsMaxInt = Mockito.mock(Utils.class);
        Mockito.when(utilsMaxInt.utilFunc2()).thenReturn(true);

        MyBranching myBranching = new MyBranching(utilsMaxInt);

        int result = myBranching.maxInt(i1, i2);

        Assertions.assertTrue(result >= 0);


        Mockito.when(utilsMaxInt.utilFunc2()).thenReturn(false);
        MyBranching myBranchingFalse = new MyBranching(utilsMaxInt);

        result = myBranchingFalse.maxInt(i1, i2);
        int max = Math.max(i1, i2);
        Assertions.assertEquals(max, result);
        System.out.println(result);
    }


    @Test
    public void ifElseExample_Test()
    {
        Utils utilsifElseExample = Mockito.mock(Utils.class);
        Mockito.when(utilsifElseExample.utilFunc2()).thenReturn(true);

        MyBranching myBranching = new MyBranching(utilsifElseExample);

        boolean result = myBranching.ifElseExample();
        Assertions.assertTrue(result);

    }

    @Test
    public void switchExample_Test0_true()
    {
        int i = 0;

        Utils utilsSwitchExample = Mockito.mock(Utils.class);

        Mockito.when(utilsSwitchExample.utilFunc2()).thenReturn(true);
        Mockito.when(utilsSwitchExample.utilFunc1(ArgumentMatchers.anyString())).thenReturn(true);

        MyBranching myBranching = new MyBranching(utilsSwitchExample);
        myBranching.switchExample(i);
        Mockito.verify(utilsSwitchExample, Mockito.times(1)).utilFunc1(ArgumentMatchers.anyString());
    }

    @Test
    public void switchExample_Test0_false()
    {
        int i = 0;

        Utils utilsSwitchExample = Mockito.mock(Utils.class);

        Mockito.when(utilsSwitchExample.utilFunc2()).thenReturn(false);
        MyBranching myBranching = new MyBranching(utilsSwitchExample);
        myBranching.switchExample(i);

        Mockito.verify(utilsSwitchExample, Mockito.times(1)).utilFunc2();
        Mockito.verify(utilsSwitchExample, Mockito.never()).utilFunc1(ArgumentMatchers.anyString());

    }

    @Test
    public void ifElseExample_Test1()
    {
        int i = 1;

        Utils utilsSwitchExample = Mockito.mock(Utils.class);
        MyBranching myBranching = new MyBranching(utilsSwitchExample);

        myBranching.switchExample(i);
        Mockito.verify(utilsSwitchExample, Mockito.times(1)).utilFunc1(ArgumentMatchers.anyString());
        Mockito.verify(utilsSwitchExample, Mockito.times(1)).utilFunc2();
    }

    @Test
    public void ifElseExample_Test2()
    {
        int i = 2;

        Utils utilsSwitchExample = Mockito.mock(Utils.class);
        MyBranching myBranching = new MyBranching(utilsSwitchExample);

        myBranching.switchExample(i);
        Mockito.verify(utilsSwitchExample, Mockito.times(1)).utilFunc2();
    }
}
